# Flaky CLI

CLI for running a command until it fails.

## Release

```sh
mkdir release
gem build flaky_cli.gemspec --output=release/flaky_cli.gem
envchain rubygems gem push release/flaky_cli.gem
```
