# frozen_string_literal: true

require_relative "lib/flaky_cli/version"

Gem::Specification.new do |spec|
  spec.name = "flaky_cli"
  spec.version = FlakyCLI::VERSION
  spec.authors = ["Codruț Constantin Gușoi"]
  spec.email = ["mail+gem.flaky_cli@codrut.pro"]

  spec.summary = "CLI for running a command until it fails."
  spec.description = "CLI for running a command until it fails."
  spec.homepage = "https://gitlab.com/sdwolfz/flaky_cli"
  spec.required_ruby_version = ">= 2.6.0"

  spec.metadata["allowed_push_host"] = "https://rubygems.org"

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = spec.homepage

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(__dir__) do
    `git ls-files -z`.split("\x0").reject do |f|
      (File.expand_path(f) == __FILE__) ||
        f.start_with?(*%w[bin/ test/ spec/ features/ .git .circleci appveyor Gemfile])
    end
  end
  spec.bindir = "exe"
  spec.executables = spec.files.grep(%r{\Aexe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  # Uncomment to register a new dependency of your gem
  # spec.add_dependency "example-gem", "~> 1.0"

  spec.add_development_dependency 'debug',            '~> 1.7'
  spec.add_development_dependency 'minitest',         '~> 5.17'
  spec.add_development_dependency 'rake',             '~> 13.0'
  spec.add_development_dependency 'rubocop',          '~> 1.42'
  spec.add_development_dependency 'rubocop-minitest', '~> 0.25'
  spec.add_development_dependency 'rubocop-rake',     '~> 0.6'
  spec.add_development_dependency 'simplecov',        '~> 0.22'
  spec.add_development_dependency 'yard',             '~> 0.9'

  # For more information and examples about making a new gem, check out our
  # guide at: https://bundler.io/guides/creating_gem.html
end
