# frozen_string_literal: true

require_relative "flaky_cli/version"

module FlakyCLI
  class Error < StandardError; end
  # Your code goes here...
end
